const express = require('express');
const Joi = require('@hapi/joi');

const app = express();

app.use(express.json());

const users = [
    { id: 1, 'name': 'UserA' },
    { id: 2, 'name': 'UserB' },
    { id: 3, 'name': 'UserC' }
];

app.get('/', (req, res) => {
    res.send('hello !!!');
});

app.get('/api/users', (req, res) => {
    res.send(users);
});

app.get('/api/users/:id', (req, res) => {
    const user = users.find(user => user.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('User not exist');

    res.send(user);
});

app.post('/api/users', (req, res) => {

    const result = validateUser(req.body);
    if (result.error) return res.status(400).send(result.error.details[0].message);

    const user = {
        id: users.length + 1,
        name: req.body.name
    }

    users.push(user);
    res.send(user);
});

app.put('/api/users/:id', (req, res) => {

    const user = users.find(user => user.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('User not exist');

    const result = validateUser(req.body);
    if (result.error) return res.status(400).send(result.error.details[0].message);
    
    user.name = req.body.name;
    res.send(user);
});

app.delete('/api/users/:id', (req, res) => {

    const user = users.find(user => user.id === parseInt(req.params.id));
    if (!user) return res.status(404).send('User not exist');
    
    const index = users.indexOf(user);
    users.splice(index, 1);

    res.send(user);
});

function validateUser(user) {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    });

    return schema.validate(user);
}

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listning to port ${port}...`));